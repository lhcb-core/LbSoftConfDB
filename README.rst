LHCb Software configuration Database 
====================================
|pipeline status| |coverage report|


Neo4j database used to store the dependencies of the LHCb software projects.


.. |pipeline status| image:: https://gitlab.cern.ch/lhcb-core/LbSoftConfDB/badges/master/pipeline.svg
                     :target: https://gitlab.cern.ch/lhcb-core/LbSoftConfDB/commits/master
.. |coverage report| image:: https://gitlab.cern.ch/lhcb-core/LbSoftConfDB/badges/master/coverage.svg
                     :target: https://gitlab.cern.ch/lhcb-core/LbSoftConfDB/commits/master
